﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RestartButton : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private Expressions _expressions;
    private Color _colorDefault;
    private Color _colorOnOver;

    void Start()
    {
        _colorDefault = _text.color;
        _colorOnOver = Color.yellow;

        SetGazedAt(false);
    }

    public void SetGazedAt(bool gazedAt)
    {
        if (gazedAt)
        {
            Debug.Log("Over");
            _text.color = _colorOnOver;
            _text.fontSize = 100;
            StartCoroutine(Сonfirmation());
        }
        else
        {
            Debug.Log("OUT");
            _text.color = _colorDefault;
            _text.fontSize = 80;
            StopAllCoroutines();
        }
    }

    public void HandleOver()
    {
        Debug.Log("Over");
        StartCoroutine(Сonfirmation());
    }

    public void HandleOut()
    {
        Debug.Log("OUT");
        StopAllCoroutines();
    }

    IEnumerator Сonfirmation()
    {
        yield return new WaitForSeconds(1f);

        _expressions.Restart();
    }
}
