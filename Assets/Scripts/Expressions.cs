﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
//using PostfixNotation;
using Random = UnityEngine.Random;

public class Expressions : MonoBehaviour
{
    [SerializeField] private GameOverScene _gameOverScene;
    [SerializeField] private RestartButton _restartButton;
    [SerializeField] private AnswersManager _answers;

    [SerializeField] private Text _correctAnswerText;
    [SerializeField] private Text _incorrectAnswerText;
    [SerializeField] private Text _levelText;
    [SerializeField] private Text _stepText;

    [SerializeField] private Text _task;
    [SerializeField] private Text[] _possibleAnswers;
    [SerializeField] private Text _answer4;
    [SerializeField] private Text _answer5;

    private int _correctAnswer, _incorrectAnswer;

    public string correctAnswerDisplay;
    public string[] _expressions;
    private int _step;
    public decimal _expressionValue;
    private decimal[] _possibleAnswersValue;
    // Use this for initialization
    private void Start()
    {
        _step = 0;
        _correctAnswer = 0;
        _incorrectAnswer = 0;

        GenerateExpression();
    }

    public void GenerateExpression()
    {
        _task.text = _expressions[_step];
        GenerateAnswers(_expressions[_step]);
    }

    private void GenerateAnswers(string expressions)
    {
        PostfixNotationExpression postfixNotation = new PostfixNotationExpression();
        _expressionValue = Math.Round(postfixNotation.result(expressions), 2, MidpointRounding.AwayFromZero);
        correctAnswerDisplay = _expressionValue.ToString();
        _possibleAnswersValue = GetPossibleAnswers(_expressionValue);

        for (int i = 0; i < _possibleAnswersValue.Length; i++)
        {
            _possibleAnswers[i].text = string.Format("{0:0.00}",_possibleAnswersValue[i]);
        }
    }

    private decimal[] GetPossibleAnswers(decimal correctAnswer)
    {
        int length = 0;
        length = _step < 10 ? 4 : 6;

        decimal[] array = new decimal[length];

        for (int i = 0; i < length; i++)
        {
            if (i == 0)
            {
                array[i] = correctAnswer;
            }
            else
            {
                if (i == 3 || i == 5)
                {
                    decimal value = Convert.ToDecimal(((float)correctAnswer + 0.1) * Random.Range(0.1f, 1f));

                    while (value == correctAnswer || array.Contains(value))
                    {
                        value = Convert.ToDecimal(((float)correctAnswer + 0.1) * Random.Range(0.1f, 1f));
                    }
                    array[i] = value;
                }
                else
                {
                    decimal value2 = Convert.ToDecimal((correctAnswer + 0.1m) * Random.Range(2, 5));

                    while (value2 == correctAnswer || array.Contains(value2))
                    {
                        value2 = Convert.ToDecimal((correctAnswer + 0.1m) * Random.Range(2, 5));
                    }
                    array[i] = value2;

                }
            }
        }

        array = ShuffleArray(array);

        return array;
    }

    private decimal[] ShuffleArray(decimal[] numbers)
    {
        decimal[] newArray = numbers.Clone() as decimal[];
        for (int i = 0; i < newArray.Length; i++)
        {
            decimal tmp = newArray[i];
            int r = Random.Range(i, newArray.Length);
            newArray[i] = newArray[r];
            newArray[r] = tmp;
        }
        return newArray;
    }

    public bool IsAnswerCorrect(decimal answer)
    {
        if (answer == _expressionValue)
        {
            GoToNextStep(1);
            return true;
        }
        else
        {
            GoToNextStep(0);

            return false;
        }
    }

    private void GoToNextStep(int value)
    {
        IncreaseStep();
        switch (value)
        {
            case 0:
                if (_step == _expressions.Length)
                {
                    GameOver();
                }
                
                IncreaseIncorrectAnswers();
                break;

            //answer is correct
            case 1:
                IncreaseCorrectAnswers();
                if (_step == _expressions.Length)
                {
                    GameOver();
                }

                break;
        }
        if (_step != _expressions.Length)
        {
            GenerateExpression();
        }
    }


    private void ShowMoreAnswers()
    {
        _answer4.gameObject.SetActive(true);
        _answer5.gameObject.SetActive(true);
    }

    private void HideMoreAnswers()
    {
        _answer4.gameObject.SetActive(false);
        _answer5.gameObject.SetActive(false);
    }

    private void IncreaseCorrectAnswers()
    {
        _correctAnswer++;
        _correctAnswerText.text = "Correct: " + _correctAnswer.ToString();
    }

    private void IncreaseIncorrectAnswers()
    {
        _incorrectAnswer++;
        _incorrectAnswerText.text = "Incorrect: " + _incorrectAnswer.ToString();
    }

    private void IncreaseStep()
    {
        _step++;
        _stepText.text = "Step: "+_step.ToString();

        //IncreaseStep();
        if (_step == 10)
        {
            ShowMoreAnswers();
        }
        if (_step < 5)
        {
            _levelText.text = "Level: easy";
            _levelText.color = Color.green;
        }
        else
        {
            if (_step < 15)
            {
                _levelText.text = "Level: med";
                _levelText.color = Color.yellow;
            }
            else
            {
                _levelText.text = "Level: hard";
                _levelText.color = Color.red;
            }
        }
    }

    public void GameOver()
    {
        _task.gameObject.SetActive(false);
        _answers.gameObject.SetActive(false);
        _gameOverScene.gameObject.SetActive(true);
        _restartButton.gameObject.SetActive(true);
    }

    public void Restart()
    {
        _restartButton.gameObject.SetActive(false);

        _step = 0;

        _levelText.text = "Level: easy";
        _levelText.color = Color.green;
        _stepText.text = "Step: " + _step.ToString();
        _correctAnswer = 0;
        _correctAnswerText.text = "Correct: " + _correctAnswer.ToString();
        _incorrectAnswer = 0;
        _incorrectAnswerText.text = "Incorrect: " + _incorrectAnswer.ToString();

        HideMoreAnswers();
        GenerateExpression();

        _task.gameObject.SetActive(true);
        _answers.gameObject.SetActive(true);
        _gameOverScene.gameObject.SetActive(false);
    }
}