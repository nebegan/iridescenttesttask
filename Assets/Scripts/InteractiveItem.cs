﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InteractiveItem : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private Collider _collider;
    [SerializeField] private Expressions _expressions;

    private Color _colorDefault;
    private Color _colorOnOver;

    void Start()
    {
        _colorDefault = _text.color;
        _colorOnOver = Color.yellow;

        SetGazedAt(false);
    }

    void OnDisable()
    {
        _text.color = _colorDefault;
        _text.fontSize = 28;
    }

    public void SetGazedAt(bool gazedAt)
    {
        if (gazedAt)
        {
            Debug.Log("Over");
            _text.color = _colorOnOver;
            _text.fontSize = 35;
            StartCoroutine(Сonfirmation());
        }
        else
        {
            Debug.Log("OUT");
            _text.color = _colorDefault;
            _text.fontSize = 28;
            StopAllCoroutines();
        }
    }

    public void HandleOver()
    {
        Debug.Log("Over");
        StartCoroutine(Сonfirmation());
    }

    public void HandleOut()
    {
        Debug.Log("OUT");
        StopAllCoroutines();
    }

    IEnumerator Сonfirmation()
    {
        yield return new WaitForSeconds(1f);

        decimal value = Decimal.Parse(_text.text);
        _expressions.IsAnswerCorrect(value);
    }

    //public void EnableCollider()
    //{
    //    if (!_collider.enabled)
    //    {
    //        _collider.enabled = true;
    //    }
    //}

    //public void DisableCollider()
    //{
    //    _collider.enabled = false;
    //}
}
